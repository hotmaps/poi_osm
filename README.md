# poi_osm

[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.8320936.svg)](https://doi.org/10.5281/zenodo.8320936)

## Repository structure
```
data                    -- containts the dataset in Geotiff format
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```


## Documentation

This dataset shows densities of a selection of Points of Interest in Europe from Open Street Map [1]. 
Included countires are : 
```
country_codes = ['AT', 'BE', 'BG', 'HR', 'CY', 'CZ', 'DK', 'EE', 'FI', 'FR', 'DE', 'GR', 'HU', 'IE', 'IT','LV', 'LT', 'LU', 'MT', 'NL', 'PL', 'PT', 'RO', 'SK', 'SI', 'ES', 'SE', 'AL', 'AD', 'AM', 'BY', 'BA', 'FO', 'GE', 'GI', 'IS', 'IM', 'XK', 'LI', 'MK', 'MD', 'MC', 'ME', 'NO', 'SM', 'RS', 'CH', 'TR', 'UA', 'GB', 'VA']
 
```

The requests of Points of Interest have been performed with the Overpass API [2] (free of charge).

The codes included in each density are listed below : 
```
'highway' = ['"highway"="motorway"', '"highway"="rest_area"'];
'parkings' = ['"parking"="surface"', '"parking"="multi-storey"', '"parking"="street_side"', '"parking"="underground"' , '"park_ride"' ];
'school' = ['"amenity"="college"', '"building"="college"', '"building"="university"', '"amenity"="university"', '"amenity"="school"' , '"amenity"="school"', '"amenity"="kindergarten"', '"amenity"="library"'];
'health'= ['"amenity"="clinic"', '"amenity"="dentist"', '"amenity"="school"' , '"amenity"="doctors"', '"amenity"="hospital"', '"amenity"="pharmacy"','"amenity"="veterinary"']; 
'cafe'= ['"amenity"="cafe"','"amenity"="ice_cream"', '"amenity"="internet_cafe"']; 
'supermarket' = ['"shop"="supermarket"', '"shop"="mall"', '"shop"= "department_store"', '"shop"= "convenience"'];
'restaurant'= ['"amenity"="restaurant"'];
'fastfood' = ['"amenity"="fast_food"'];
'sport'= ['"sport"']; 
'hotel' = ['"tourism"="hotel"', '"building"="hotel"', '"tourism"="guest_house"','"tourism"="apartment"','"tourism"="hostel"','"tourism"="motel"','"tourism"="camp_site"']; 
'pubs' = ['"amenity"="bar"','"amenity"="pub"', '"amenity"="biergarten"'];
'theatre'= ['"amenity"="theatre"', '"amenity"="cinema"', '"amenity"="music_venue"', '"leisure"="stadium"' ]; 
'night' = ['"amenity"="nightclub"', '"amenity"="casino"','"amenity"="gambling"','"amenity"="stripclub"']; 
'socio'= ['"amenity"="arts_centre"', '"amenity"="community_centre"', '"amenity"="social_centre"', '"amenity"="music_school"', '"amenity"="language_school"']; 
'shop' = ['"shop"'];
'tourism' = ['"amenity"="exhibition_centre"', '"tourism"="attraction"','"tourism"="viewpoint"','"tourism"="aquarium "','"leisure"="beach_resort "','"tourism"="gallery"','"tourism"="museum"','"tourism"="theme_park"','"tourism"="zoo"','"tourism"="artwork"'];
```

The pixel values are the sum of the number of POIs of each type located in the pixel.

### Limitations of the dataset
- The dataset provids densities of only a selection of points of interests. The complete list of amenity codes can be fund on the [OSM Wiki](https://wiki.openstreetmap.org/wiki/Key:amenity#Sustenance). 
- Ways are only considered throught their center points.


### References
[1] [Open Street Map](https://www.openstreetmap.org/)
[2] [Overpass API](http://overpass-api.de/api/interpreter)
[3] [OSM Wiki](https://wiki.openstreetmap.org/wiki/Key:amenity#Sustenance)



## How to cite
Jeannin Noémie, OPENGIS4ET Project WP3


## Authors
Jeannin Noémie <sup>*</sup>

<sup>*</sup> [EPFL, PV-Lab](https://www.epfl.ch/labs/pvlab/), Laboratory of photovoltaics and thin-films electronics, Rue de la Maladière 71b, CH-2002 Neuchâtel 2


## License
Copyright © 2023: Noémie Jeannin
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html

## Acknowledgment
We would like to convey our deepest appreciation to the OPENGIS4ET Project, which provided the funding to carry out the present investigation.